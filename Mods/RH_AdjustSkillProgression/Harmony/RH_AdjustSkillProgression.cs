﻿using DMT;
using Harmony;
using System;
using System.Reflection;
using UnityEngine;

public class RH_AdjustSkillProgression
{
    public class RH_AdjustSkillProgression_Init : IHarmony
    {
        public void Start()
        {
            Debug.Log(" Loading Patch : " + GetType().ToString());
            var harmony = HarmonyInstance.Create(GetType().ToString());
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }

    [HarmonyPatch(typeof(Progression))]
    [HarmonyPatch("AddLevelExpRecursive")]
    [HarmonyPatch(new Type[] { typeof(int), typeof(string) })]
    public class PatchProgressionAddLevelExpRecursive
    {
        static bool Prefix(Progression __instance, ref int exp, ref string _cvarXPName)
        {
            if (__instance.Level >= Progression.MaxLevel)
            {
                if (__instance.Level > Progression.MaxLevel)
                {
                    __instance.Level = Progression.MaxLevel;
                }

                // Max level so dont even need to run the original method!
                return false;
            }

            var nextLevel = __instance.Level + 1;
            var xpLeftToGo = __instance.ExpToNextLevel - exp;

            if (nextLevel % 10 == 0 && xpLeftToGo <= 0)
            {
                __instance.SkillPoints += 4;
            }

            return true;
        }
    }
}

