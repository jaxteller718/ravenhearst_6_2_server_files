﻿using System;
using Harmony;
using UnityEngine;
using System.Reflection;
using DMT;
using System.Collections.Generic;

public class RH_GameOptionDisableQuestSatchelIcon
{
    public class RH_GameOptionDisableQuestSatchelIcon_Init : IHarmony
    {
        public void Start()
        {
            Debug.Log(" Loading Patch : " + GetType().ToString());
            var harmony = HarmonyInstance.Create(GetType().ToString());
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }

    [HarmonyPatch(typeof(MapObjectHiddenCache))]
    [HarmonyPatch("IsOnCompass")]
    public class PatchMapObjectHiddenCacheIsOnCompass
    {
        public static bool Prefix(MapObjectHiddenCache __instance, ref bool __result)
        {
            __result = GameStats.GetBool(EnumGameStats.ShowFetchItemOnCompass);
            return false;
        }
    }

    [HarmonyPatch(typeof(MapObjectFetchItem))]
    [HarmonyPatch("IsOnCompass")]
    public class PatchMapObjectFetchItemIsOnCompass
    {
        public static bool Prefix(MapObjectHiddenCache __instance, ref bool __result)
        {
            __result = GameStats.GetBool(EnumGameStats.ShowFetchItemOnCompass);
            return false;
        }
    }
}
