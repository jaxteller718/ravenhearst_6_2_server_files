﻿using Harmony;
using System.Reflection;
using UnityEngine;
using DMT;
using System;

namespace RH_LootWindowSearch
{
    public class RH_LootWindowSearch_Init : IHarmony
    {
        public void Start()
        {
            Debug.Log(" Loading Patch : " + GetType().ToString());
            var harmony = HarmonyInstance.Create(GetType().ToString());
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }

    [HarmonyPatch(typeof(XUiC_ItemStack))]
    [HarmonyPatch("Update")]
    [HarmonyPatch(new Type[] { typeof(float) })]
    public class PatchXUiC_ItemStackUpdate
    {
        static void Postfix(XUiC_ItemStack __instance, ref float _dt)
        {
            if (__instance.WindowGroup.isShowing && !__instance.IsDumpLocked && !__instance.isOver && __instance.SearchMatch)
            {
                __instance.background.Color = Color.cyan;
            }
        }
    }
}
