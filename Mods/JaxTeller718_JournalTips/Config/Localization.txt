﻿Key,Source,Context,English
microwaveTip,Journal,EnChanged,The microwave can be placed and used to heat canned food recipes. Canned food recipes offer slightly more benefits to them than if you eat them in singles so save those cans when you can. \nBowls can be found in loot and are used with the canned foods.,
microwaveTip_title,Journal,KeyTrunk,Microwave

dryingRackRHTip,Journal,EnChanged,"The drying rack is used to convert animal hides you get from animals into leather. It can also be useful in making survival foods such as jerky and trail mix."
dryingRackRHTip_title,Journal,KeyTrunk,Drying Rack

armorBenchRHTip,Journal,EnChanged,"An armor bench is crucial to welding metallic armor and making clothing."
armorBenchRHTip_title,Journal,KeyTrunk,Armor Bench

waterFiltrationUnitRHTip,Journal,EnChanged,"All water sources have been contaminated. Even boiling no longer works. The water filtration unit is designed to remove all remaining pathogens from water so that you can drink reduced risk of contamination. \n\nYou can also use the water filtration unit to allow for safe planting of crops via use of a bucket of water that has been decontaminated. \n\nWith the proper knowledge eventually you will be able to purify water and drink safely with no risk of dysentery."
waterFiltrationUnitRHTip_title,Journal,KeyTrunk,Purifying Water

artisanLabRHTip,Journal,EnChanged,"The artisan table is a dye making table where you can use paint to mix all new colors for use in clothing and tools and weapons."
artisanLabRHTip_title,Journal,KeyTrunk,Dying Clothes

farmTableRHTip,Journal,EnChanged,"The farming table is crucial to gardening and farm making. In it you can cultivate seeds from crops. You can also make fertilizer using this table."
farmTableRHTip_title,Journal,KeyTrunk,Farming Table

personalBenchTip,Journal,EnChanged,"The personal craft station is your one stop shop for all your crafting needs. It can easily be picked up by destroying it and is not expensive to craft."
personalBenchTip_title,Journal,KeyTrunk,Personal Craft Station

decorTableRHTip,Journal,EnChanged,"The decorating table is where you can go crazy with inspiration and use RH exclusive blocks to decorate your home."
decorTableRHTip_title,Journal,KeyTrunk,Decorating Table

ammunitionTableRHTip,Journal,EnChanged,"Make all of your ammo in this table."
ammunitionTableRHTip_title,Journal,KeyTrunk,Ammunition Press

beeHiveRHTip,Journal,EnChanged,"Now that you have found some bees you can put them to work in a beehive. \n\nCraft a beehive and use the bees to populate it. Over time the bees will produce honeycombs for you to turn into honey. \n\nYou can repeat this process as many times as you like by refilling the beehive with bees."
beeHiveRHTip_title,Journal,KeyTrunk,Producing Honey

TurdRHTip,Journal,EnChanged,"Turds can be quite valuable as a resource in farming. When you use turds along with spoiled food and other ingredients you can create compost. Compost is used in Composters and will create fertilizer over time. \nFertilizer is used as a second way to create farm plots. When your composter is ready you will receive various amounts of fertilizer and turds back. Using the fertilizer and composter you can create a renewable resource for your farm plots to grow your crops in. \n\nDon't forget that farm plots need to be within one block length of water in order to grow successfully. \n\nCraft a beehive and use the bees to populate it. Over time the bees will produce honeycombs for you to turn into honey. \n\nYou can repeat this process as many times as you like by refilling the beehive with bees."
TurdRHTip_title,Journal,KeyTrunk,Compost and Fertilizer

foodSpoilageRHTip,Journal,EnChanged,"All spoilage times are for in your backpack. Toolbelt spoils fastest. Containers take longer. Blue Coolers are next, followed by Refrigerators. The Preservation Barrel has slowest spoilage time. \n\nAll spoilage times are for backpacks. Minutes are in world minutes, it is calculated using real world time. \n\nRaw Meat and Fish \nSpoilage Time: Lose 1 Piece every 60 minutes \n\nEggs and Cooked Dishes \nSpoilage Time: Lose 1 Piece every 90 minutes \n\nBread, Baked Goods \nSpoilage Time: 1 piece every 120 minutes \n\nCrops and Fruit \nSpoilage Time: 1 piece every 120 minutes \n\nCanned Food and Ingredients \nNo Spoilage Time \n\nYou can loot cling wrap that you can use to seal raw meat. Sealed meat has no expiration time. Calculations are done at the time of opening a container. You may see multiple pieces expire at once."
foodSpoilageRHTip_title,Journal,KeyTrunk,Food Spoilage

brokenStationRHTip,Journal,EnChanged,"As you venture in the world you will notice many of the workstations you come across may be broken. Fear not because there is a way to fix them and restore their use. All stations have an upgrade kit that can be crafted. The materials to make them are usually similar to the station itself. Using a hammer and the appropriate kit you can take a broken station and make it function again."
brokenStationRHTip_title,Journal,KeyTrunk,Upgrading Broken Stations

gasRHTip,Journal,EnChanged,"Gas is an essential resource in the apocalypse but it can be tough to get your hands on it. You will find empty gas cans in vehicles and auto shops. You can even craft them yourself. These empty gas cans can be taken to a working gas pump. There you will use 50 cans on it to initiate gas production. In a few hours you will be able to loot the gas pump and take the gas that was left behind home with you. \n\nYou may also produce gas by digging for shale in the desert."
gasRHTip_title,Journal,KeyTrunk,Gas Production

qualityRHTip,Journal,EnChanged,"You will notice that all items have a durability. This is a numbered range from 1-120. The higher the durability the longer the item will last and the better the stats will be. You can raise the quality at which you craft these items by using the item and levelling the Action Skill associated with it. \n\nRepairing an item will cost you 10 points in durability unless you use a workbench to combine two items for repair. Doing this will combine both items durability numbers to create a new one. \n\nYou can lessen the point hit on Quality by raising your Construction Skill. For every 20 levels of Construction you gain you minimize the point reduction on Quality by 1 point. \n\nConstruction Skills 19 and below = 10 point loss \nConstruction Tools 20 = 9 point loss \nConstruction Skills 30 = 8 point loss\nConstruction Skills 40 = 7 point loss \nConstruction Skills 50 = 6 point loss \nAnd so on until you hit 100 for a 1 point loss \n\nPlease note that items that are under 11 Quality will break when you try to repair them and you must make another one."
qualityRHTip_title,Journal,KeyTrunk,Item Quality and Durability

researchDeskRHTip,Journal,EnChanged,"With a research desk you can use the ink you have collected with the knowledge you have gained to learn new recipes and perk books. Very handy for filling in those missing gaps in your collection."
researchDeskRHTip_title,Journal,KeyTrunk,Research Desk

autoWorkbenchRHTip,Journal,EnChanged,"With the car parts you have found around the world and the workbench you can assemble a workstation that will allow you to build any vehicle from the ground up. These can also be found scattered around the world in mechanic shops and gas stations but they may not always work."
autoWorkbenchRHTip_title,Journal,KeyTrunk,Automotive Bench

labTableRHTip,Journal,EnChanged,"The lab table you make from doing the Weird Science quest line will allow you to mix and concoct all sorts of special formulas and science-y type things."
labTableRHTip_title,Journal,KeyTrunk,Weird Science